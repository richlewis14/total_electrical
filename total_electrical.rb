require 'bundler/setup'
Bundler.require(:default)
require 'sinatra'
require 'sinatra/flash'
require 'sinatra/static_assets'
require 'pony'

configure :production do
  require 'newrelic_rpm'
end

set :static_cache_control, [:public, max_age: 60 * 60 * 24 * 365]
enable :sessions

get '/' do
  erb :index
end

post '/' do
    from = params[:name]
    subject = "#{params[:name]} has contacted you via Total Electrical"
    body = erb(:mail, layout: false)

  Pony.mail(
  :from => from,
  :to => ENV["EMAIL_ADDRESS"],
  :subject => subject,
  :body => body,
  :via => :smtp,
  :via_options => {
    :address              => 'smtp.gmail.com',
    :port                 => '587',
    :enable_starttls_auto => true,
    :user_name            => ENV["USER_NAME"],
    :password             => ENV["PASSWORD"],
    :authentication       => :plain, 
    :domain               => "localhost.localdomain" 
})
  flash[:notice] = "Thanks for your email. We will be in touch soon."
  redirect '/success' 

end

get '/success' do
  erb :index
end
