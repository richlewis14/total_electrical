(function($) {
  "use strict";

  function validate(){
      if ($('#name').val().length   >   0   &&
          $('#email').val().length  >   0   &&
          $('#phone').val().length  >   0   &&
          $('#message').val().length    >   0) {
          $("input[type=submit]").prop("disabled", false);
      }
      else {
        $("input[type=submit]").prop("disabled", true);
      }
  }

  jQuery(document).ready(function($) { 

    //Contact Form Button inative until all fields filled in
    validate();
    $('#name, #email, #phone, #message' ).change(validate);

    // Honey Trap
     $('.inputGhost').on('change', function() {
       $("input[type=submit]").prop('disabled', $(this).val().length > 0);
     });

    //Flash Fadeout
    $('#flash').fadeOut(5000);

    // Read More/Less Links
    $(".read-more").hide();
    $(".read").click(function(e){
      e.preventDefault();
      $(this).parent().children(".read-more").slideToggle("slow");
       $(this).text($(this).text() == 'Read less...' ? 'Read more...' : 'Read less...');
    });
  

jQuery(document).ready(function($){    
     SidebarMenuEffects();      
    // cache container
    var $container = $('#filtercontainer');
    // initialize isotope
    $container.imagesLoaded( function(){
        $container.isotope({ 
          masonry: { columnWidth: $container.width() / 4 },
          filter: '.filteritem',
          animationOptions: {
               duration: 750,
               queue: false
             }
           });
     });

    $(window).smartresize(function(){
  $container.isotope({
    // update columnWidth to a percentage of container width
    masonry: { columnWidth: $container.width() / 4 }
  });
});

   
        
    });
      
jQuery(document).ready(function($) { 
 // Cache selectors
 var lastId;
 var topMenu = $(".scrollmenu"); 
 var topMenuHeight = 70;//topMenu.outerHeight()+15
     // All list items
 var menuItems = topMenu.find("a"),
     // Anchors corresponding to menu items
     scrollItems = menuItems.map(function(){
       var item = $($(this).attr("href"));
       
       if (item.length) { return item; }
     });
  

 // Bind click handler to menu items
 // so we can get a fancy scroll animation
menuItems.click(function(e){
  e.preventDefault();
   var href = $(this).attr("href"),
       offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
       
  
   $('html, body').stop().animate({ 
       scrollTop: offsetTop
   }, 400);
  //return false;
 });


        $(window).scroll( function ()
        {
            var fromTop = $(this).scrollTop()+topMenuHeight+20;
            var cur = scrollItems.map(function(){
              if ($(this).offset().top < fromTop)
                return this;
            });
            cur = cur[cur.length-1];
            var id = cur && cur.length ? cur[0].id : "";
            if (lastId !== id) {
                lastId = id;
                menuItems
                  .parent().removeClass("active");
                  menuItems.filter("[href=#"+id+"]").parent().addClass("active");               
                   }
                   
                 
        });
    });
  jQuery(window).load(function() {    
    var screenheight=jQuery(window).height()-70;
    
    $('#home').css('height',screenheight+'px');
    $('#homecontent').css('height',screenheight+'px');
    $('.homeslider').css('height',screenheight+'px');
    
    jQuery(window).scroll(function(){
       var wtop= jQuery(window).scrollTop();
       
       if(wtop > screenheight){
        jQuery('header').addClass('fix');
       }else{
        jQuery('header').removeClass('fix');
       }
    });

    $('.parallax').each(function(){
        var $bgobj = $(this);
        $(window).scroll(function(e) {
            e.preventDefault();
            var $window = jQuery(window);
            var yPos = -Math.round(($window.scrollTop()/4));
            var coords;
            coords = '50% '+yPos + 'px';
            // Move the background
            $bgobj.css({backgroundPosition: coords});
        }); 
      });
});    



      
});   


})(jQuery);       